<?php
/**
 * Created by PhpStorm.
 * User: Oleg
 * Date: 28.01.2018
 * Time: 22:11
 */
namespace AppBundle\Form;
use AppBundle\Entity\Film;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
class FilmType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titre', TextType::class, [
                'attr' => [
                    'autofocus' => true,
                    'class' => 'form-control'
                ],
                'label' => 'Titre'

            ])
            ->add('genre', ChoiceType::class, [
                'attr' =>[
                    'class' => 'form-control'
                ],
                'choices'  => array(
                    'Comedie' => 'comedie',
                    'Action' => 'action',
                    'Romance' => 'romance',
                    'Drama' =>'drama'
                ),
                'required' => true,
                'label' => 'Genre du Film',
            ])
           
            ->add('realisateur', TextType::class, [
                'attr' =>[
                    'class' => 'form-control'
                ],
                'label' => 'Nom Realisateur',
            ])
            ->add('acteur', TextType::class, [
                'attr' =>[
                    'class' => 'form-control'
                ],
                'label' => 'Nom Acteur',
            ])
            ->add('annee', DateType::class, [
                #'format' => 'D-M-Y H:M',
                'attr' =>[
                    'class' => 'form-control'
                ],
                'widget' => 'single_text',
                'html5' => true,
                'label' => 'Annee',
            ])
            ->add('image', FileType::class, [
                'label' => 'Image',
                'data_class' => null,
                'required' => false,
                'label_attr' => [
                    //'class' => 'custom-file-label'
                ],
                'attr' => [
                    'class' => 'form-control-file',
                ],
            ])
            
          /*  ->add('save', SubmitType::class, array(
                'label' => 'Create',
                'attr' => ['class' => 'btn-primary'],
            ))*/
        ;
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Film::class,
        ]);
    }
}