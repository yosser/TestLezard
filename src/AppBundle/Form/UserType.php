<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;

class UserType extends AbstractType
{   /**
     * {@inheritdoc}
     */

    public function buildForm(FormBuilderInterface $builder, array $options){
        $builder
            ->add('username', TextType::class, [
                'label' => 'Username',
                'attr' => [
                    'placeholder' => 'username',
                    'class' =>'form-control'
                ],
            ])
            ->add('username_canonical', TextType::class, [
                'label' => 'Username_canonical',
                'attr' => [
                    'placeholder' => 'username_canonical',
                    'class' =>'form-control'
                ],
            ])
            
            ->add('email', EmailType::class, [
                'label' => 'Email',
                'attr' => [
                    'placeholder' => 'email',
                    'class' =>'form-control'
                ],
            ])
             ->add('email', EmailType::class, [
                'label' => 'Email_Canonical',
                'attr' => [
                    'placeholder' => 'email_canonical',
                    'class' =>'form-control'
                ],
            ])

            ->add('num_telephone', TextType::class, [
                'label' => 'Numéro De Téléphone',
                'attr' => [
                    'placeholder' => 'numero de telephone',
                    'class' =>'form-control'
                ],
            ])
            
            ->add('image', FileType::class, [
                'label' => 'Ajouter Image',
                'data_class' => null,
                'required' => false,
                'label_attr' => [
                    //'class' => 'custom-file-label'
                ],
                'attr' => [
                    'class' => 'form-control-file',
                ],
            ]);
    }
  
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User'
        ));
    }

    /**
     * {@inheritdoc}
     *
     */
    public function getBlockPrefix()
    {
        return 'appbundle_user';
    }


}
