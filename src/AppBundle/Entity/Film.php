<?php

namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="films")
 */
class Film
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string titre
     *
     * @ORM\Column(name="titre", type="string", nullable=false, length=255)

    **/

    private $titre;
    /**
     * @var string genre
     * @Assert\Choice(choices={"comedie", "action","romance","drama"}, message="Choose a valid genre.")

     * @ORM\Column(name="genre", type="string", nullable=false, length=255)
     **/

    
    private $genre;

     /**
     *@var date annee
     *@ORM\Column(name="annee", type="date", nullable=false)
     * @Assert\Date()
     **/
    private $annee;

     /**
     * @var string realisateur
     
     * @ORM\Column(name="realisateur", type="string", nullable=false, length=255)

    **/
    private $realisateur;

     /**
     * @var string acteur
     *
     * @ORM\Column(name="acteur", type="string", nullable=false, length=255)

    **/

     private $acteur;
     /**
     *
     * @ORM\Column(type="string", nullable=true)
     * @Assert\File(mimeTypes={ "image/jpeg" })
     */

    private $image = "";

       /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="films")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;


      public function __construct()
      {
        $this->annee = new \Datetime();
      }
  


    public function getId(){

        return $this->id;
    }
    

    public function getUser(){
       return  $this->user;
    }

    public function setUser($user){
        $this->user=$user;

    }
    public function getTitre(){

       return $this->titre;
    }

    public function getGenre(){

        return $this->genre;
    
    }
    
    public function getRealisateur(){

        return $this->realisateur;
    }

    public function getActeur(){

       return  $this->acteur;
    }
    public function getImage()
    {
        $image = $this->image;
        if(empty($image))
            $image = "no_avatar.jpg";
        return $image;
    }


     public function getAnnee(){

        return $this->annee;
    }



    public function setTitre($titre){

        $this->titre=$titre;
    }

    public function setGenre($genre){

        $this->genre=$genre;
    
    }
    
    public function setRealisateur($nom_realisateur){

        $this->realisateur=$nom_realisateur;
    }
    public function setActeur($acteur){

        $this->acteur=$acteur;
    }
    public function setImage($image){

        $this->image=$image;
    }

    public function setAnnee($annee){

          $this->annee=$annee;
    }
     public function __toString() {
            return (string) $this->titre; }

}

?>