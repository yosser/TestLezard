<?php
// src/AppBundle/Entity/User.php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as FosUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends FosUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

     /**
     * @var integer $num_telephone
     *
     * @ORM\Column(name="num_telephone", type="integer", nullable=true)
     */
    private $num_telephone;

    /* @ORM\Column(type="string")
     *
     * @Assert\NotBlank(message="Ajouter une image jpg")
     * @Assert\File(mimeTypes={ "image/jpeg" })
     */
    private $image = "";
     /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Film", mappedBy="user")
     */
    protected $films;

    public function __construct()
    {
        parent::__construct();

        $this->films = new ArrayCollection();
    
    }
     
    public function getFilms()
    {
        return $this->films;
    }

    public function setEvents(ArrayCollection $films)
    {
        $this->films = $films;
    }

    public function getNumTelephone()
    {
        return $this->num_telephone;
    }
    
    public function getImage(){

        $image = $this->image;
        if(empty($image))
            $image = "no_avatar.jpg";
        return $image;
    }
    /**
     * Set NumTelephone
     *
     * @param String $num_telephone
     * @return User
     */
    public function setNumTelephone($num_telephone)
    {
        return $this->num_telephone = $num_telephone;
    
    
    }
     /**
     * Set Image
     *
     * @param String $image
     * @return User
     */
    public function setImage($image)
    {
        return $this->image = $image;
    
    }

    
    
}
