<?php
/**
 * Created by PhpStorm.
 * User: Oleg
 * Date: 28.01.2018
 * Time: 18:42
 */
namespace AppBundle\Controller;
use AppBundle\Entity\User;
use AppBundle\Form\UserType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
/**
 * Class ResponsableController
 * @package AppBundle\Controller
 * @Route("/user")
 * @Security("has_role('ROLE_USER')")
 */
class ResponsableController extends Controller
{
  
    /**
     * @Route("/{id}/edit", requirements={"id": "\d+"}, name="user_edit")
     * @Method({"GET", "POST"})
     */
    public function editUserAction(Request $request, User $user){
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            $this->addFlash('success', 'user.updated_successfully');
            return $this->redirectToRoute('user_edit');
        }
        return $this->render('user/edit.html.twig', array(
            'user' => $user,
            'form' => $form->createView(),
        ));
    }
}