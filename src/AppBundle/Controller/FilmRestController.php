<?php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use AppBundle\Entity\Film;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;



class FilmRestController extends FOSRestController
{
//rest film get All Films : done
    /**
     * @Rest\Get("/films")
     */
    public function getAction()
    {
      $restresult = $this->getDoctrine()->getRepository('AppBundle:Film')->findAll();
        if ($restresult === null) {
          return new View("there are no films exist", Response::HTTP_NOT_FOUND);
     }
        return $restresult;
    }

      /**
	 * @Rest\Get("/film/{id}")
	 */
	 public function idAction($id)
	 {
	   $singleresult = $this->getDoctrine()->getRepository('AppBundle:Film')->find($id);
	   if ($singleresult === null) {
	   return new View("Film not found", Response::HTTP_NOT_FOUND);
	   }
	 return $singleresult;
	 }

	/**
		 * @Rest\Post("/film/")
	*/
	public function postAction(Request $request)
	 {
		$data  = new Film();
		   $titre = $request->get('titre');
		   $genre = $request->get('genre');
		   $realisateur = $request->get('realisateur');
		   $annee = $request->get('annee');
		   $acteur = $request->get('acteur');
		   $image = $request->get('image');

		 if(empty($titre) || empty($genre)|| empty($realisateur)|| empty($acteur)|| empty($annee))
		 {
		   return new View("NULL VALUES ARE NOT ALLOWED", Response::HTTP_NOT_ACCEPTABLE); 
		 } 
		  $data->setTitre($titre);
		  $data->setGenre($genre);
		  $data->setRealisateur($realisateur);
		  $data->setAnnee(new\DateTime());
		  $data->setActeur($acteur);
		  $data->setImage($image);
		  $data->setUser($this->getUser());
		  $em = $this->getDoctrine()->getManager();
		  $em->persist($data);
		  $em->flush();
		return new View("Film Added Successfully", Response::HTTP_OK);
	}

	  /**
	 * @Rest\Put("/film/{id}")
	 */
	public function updateAction($id,Request $request)
	 { 
		 $data = new Film();
			 $titre = $request->get('titre');
			 $genre = $request->get('genre');
			 $acteur = $request->get('acteur');
			 $realisateur = $request->get('realisateur');
			 $image = $request->get('image');
			 $annee = $request->get('annee');
			 $sn = $this->getDoctrine()->getManager();
			 $film = $this->getDoctrine()->getRepository('AppBundle:Film')->find($id);
		if (empty($film)) {
		   return new View("film not found", Response::HTTP_NOT_FOUND);
		 } 
		elseif(!empty($titre) && !empty($genre) && !empty($acteur) && !empty($realisateur) && !empty($annee)){
		   $film->setTitre($titre);
		   $film->setGenre($genre);
		   $film->setActeur($acteur);
		   $film->setRealisateur($realisateur);
		   $film->setAnnee(new\DateTime());
		   $sn->flush();
		   return new View("Film Updated Successfully", Response::HTTP_OK);
		 }
		elseif(empty($titre) && !empty($genre)){
		   $film->setGenre($genre);
		   $sn->flush();
		   return new View("genre Updated Successfully", Response::HTTP_OK);
		}
		elseif(!empty($titre) && empty($genre)){
		 $film->setTitre($titre);
		 $sn->flush();
		 return new View("Film Title Updated Successfully", Response::HTTP_OK); 
		}
		elseif(!empty($acteur) && empty($realisateur)){
		 $film->setActeur($acteur);
		 $sn->flush();
		 return new View("Actor Updated Successfully", Response::HTTP_OK); 
		}
		elseif(empty($acteur) && !empty($realisateur)){
		 $film->setRealisateur($realisateur);
		 $sn->flush();
		 return new View("Reaalisateur Updated Successfully", Response::HTTP_OK); 
		}
		elseif(!empty($annee)){
		 $film->setAnnee($annee);
		 $sn->flush();
		 return new View("Year Updated Successfully", Response::HTTP_OK); 
		}
		else return new View("Film title or actor or realisator or year or genre cannot be empty", Response::HTTP_NOT_ACCEPTABLE); 
	}
	/**
	 * @Rest\Delete("/film/{id}")
	*/
	public function deleteAction($id)
	{
		$data = new Film();
			$sn = $this->getDoctrine()->getManager();
			$film = $this->getDoctrine()->getRepository('AppBundle:Film')->find($id);
			if (empty($film)) 
			{
			  return new View("film not found", Response::HTTP_NOT_FOUND);
			}
			else 
			{
			  $sn->remove($film);
			  $sn->flush();
			}
	    return new View("film deleted successfully", Response::HTTP_OK);
	}

}

?>