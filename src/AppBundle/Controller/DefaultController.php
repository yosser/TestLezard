<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\contactType;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/user/test", name="UserPage")
     */
    public function TestRoleUserAction(Request $request)
    {   $this->denyAccessUnlessGranted('ROLE_USER');
        // replace this example code with whatever you need
        return $this->render('Roles/hello-world.html.twig');
    }

     /**
     * @Route("/admin/test", name="AdminPage")
     */
    public function TestRoleAdminAction(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $user=$this->getUser()->setEmail('ch_yosser@outlook.sa');
        $em=$this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        // replace this example code with whatever you need
        return $this->render('Roles/hello-world-admin.html.twig');
    }
    
    public function contactAction(Request $request)
    {
        // Create the form according to the FormType created previously.
        // And give the proper parameters
        $form = $this->createForm('AppBundle\Form\contactType',null,array(
            // To set the action use $this->generateUrl('route_identifier')
            'action' => $this->generateUrl('myapplication_contact'),
            'method' => 'POST'
        ));

        if ($request->isMethod('POST')) {
            // Refill the fields in case the form is not valid.
            $form->handleRequest($request);

            if($form->isValid()){
                // Send mail
                if($this->sendEmail($form->getData())){

                    // Everything OK, redirect to wherever you want ! :
                    
                    return $this->redirectToRoute('homepage');
                }else{
                    // An error ocurred, handle
                    var_dump("Errooooor :(");
                }
            }
        }

        return $this->render('contact.html.twig', array(
            'form' => $form->createView(),
        ));
    }
    
    private function sendEmail($data){
        $myappContactMail = 'cherniyosser95@gmail.com';
        $myappContactPassword = 'lifeisbitch';
        
        // In this case we'll use the ZOHO mail services.
        // If your service is another, then read the following article to know which smpt code to use and which port
        // http://ourcodeworld.com/articles/read/14/swiftmailer-send-mails-from-php-easily-and-effortlessly
        $transport = \Swift_SmtpTransport::newInstance('smtp.gmail.com', 465,'ssl')
            ->setUsername($myappContactMail)
            ->setPassword($myappContactPassword);

        $mailer = \Swift_Mailer::newInstance($transport);
        
        $message = \Swift_Message::newInstance( $data["subject"])
        ->setFrom(array($myappContactMail => "Message by ".$data["name"]))
        ->setTo(array(
            $myappContactMail => $myappContactMail
        ))
        ->setBody($data["message"]." Send Via : " .$data["email"]);
        
         return $mailer->send($message);
    }
    /**
     * 
     *
     * @Route("/AboutUs", name="AboutUs")
     * 
     */
    public function AboutUsPage(){
        return $this->render('default/about.html.twig');


    }
    
}

