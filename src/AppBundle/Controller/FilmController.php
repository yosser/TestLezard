<?php
/**
 * Created by PhpStorm.
 * User: Oleg
 * Date: 28.01.2018
 * Time: 18:42
 */
namespace AppBundle\Controller;
use AppBundle\Entity\Film;
use AppBundle\Form\FilmType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
/**
 * Class fILMController
 * @package AppBundle\Controller
 *
 * @Route("/user/film")
 * @Security("has_role('ROLE_USER')")
 */
class FilmController extends Controller
{
    /**
     * List all Films
     *
     * @Route("/user/films", name="user_film_index")
     */
    public function indexAction(Request $request){
        $films = $this->getDoctrine()
            ->getRepository(Film::class)
            ->findAll();
        return $this->render('user/film/index.html.twig', array(
            'films' => $films,
        ));


    }
    /**
     * @Route("/{id}/detail", name="user_film_view")
     * 
     */
    public function viewFilmAction($id)
    {
        $film = $this->getDoctrine()
        ->getRepository(Film::class)
        ->find($id);
        if (empty($film))
        {
            $this->addFlash('error', 'Todo not found');
        return $this->redirectToRoute('user_film_index');
        }
        return $this->render('user/film/view.html.twig', array(
            'film' => $film,
        ));
    }      
    
    /**
     * Create new film
     *
     * @Route("/create", name="user_film_create")
     * @Method({"GET", "POST"})
     */
    public function createFilmAction(Request $request){
        $film = new Film();
        $film->setUser($this->getUser());
        $form = $this->createForm(FilmType::class, $film);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            //$user = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($film);
            $em->flush();
            $this->addFlash('success', 'film created_successfully');
            return $this->redirectToRoute('user_film_index');
        }
        return $this->render('user/film/create.html.twig', array(
            'form' => $form->createView(),
        ));
    }
    /**
     * @Route("/{id}/edit", requirements={"id": "\d+"}, name="user_film_edit")
     * @Method({"GET", "POST"})
     */
    public function editFilmAction(Request $request, Film $film){
        $form = $this->createForm(FilmType::class, $film);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            //$user = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($film);
            $em->flush();
            $this->addFlash('success', 'film.updated_successfully');
            return $this->redirectToRoute('user_film_index');
        }
        return $this->render('user/film/edit.html.twig', array(
            'film' => $film,
            'form' => $form->createView(),
        ));
    }
    /**
     * @Route("/{id}/delete", name="user_film_delete")
     * @Method("POST")
     */
    public function deleteFilmAction(Request $request, Film $film){
        if (!$this->isCsrfTokenValid('delete', $request->request->get('token'))) {
            return $this->redirectToRoute('user_film_index');
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($film);
        $em->flush();
        $this->addFlash('success', 'film.deleted_successfully');
        return $this->redirectToRoute('user_film_index');
    }
}